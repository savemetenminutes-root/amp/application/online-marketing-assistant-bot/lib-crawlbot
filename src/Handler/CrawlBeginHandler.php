<?php

declare(strict_types=1);

namespace Smtm\Crawlbot\Handler;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Smtm\Crawlbot\Form\IndexFormDecorator;
use Smtm\Crawlbot\Model\Entity\Crawlbot as EntityCrawlbot;
use Smtm\Crawlbot\Service\Crawlbot;
use Zend\Diactoros\Response\HtmlResponse;
use Zend\Diactoros\Response\JsonResponse;
use Zend\Expressive\Plates\PlatesRenderer;
use Zend\Expressive\Router;
use Zend\Expressive\Template;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Expressive\Twig\TwigRenderer;
use Zend\Expressive\ZendView\ZendViewRenderer;
use Zend\I18n\Translator\Translator;

class CrawlBeginHandler implements RequestHandlerInterface
{
    private   $containerName;
    private   $router;
    private   $template;
    protected $translator;
    protected $indexForm;
    protected $crawlbot;

    public function __construct(
        TemplateRendererInterface $template,
        Router\RouterInterface $router,
        Translator $translator,
        IndexFormDecorator $indexForm,
        Crawlbot $crawlbot
    ) {
        /** @var \Zend\Expressive\ZendView\ZendViewRenderer $template */
        $this->template      = $template;
        $this->router        = $router;
        $this->translator    = $translator;
        $this->indexForm     = $indexForm;
        $this->crawlbot      = $crawlbot;
    }

    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->indexForm->setData($request->getQueryParams());

        $viewModelVars = [
            'form' => $this->indexForm,
        ];

        // TODO: Add proper URL validation which rejects schemeless urls
        if (! $this->indexForm->isValid()) {
            return new HtmlResponse($this->template->render('crawlbot::index', $viewModelVars));
        }

        $crawlSettings = $this->indexForm->getData();

        // TODO: Use a custom InputFilter for the dbTableSuffix
        $dbTableSuffix = ($crawlSettings->getDbTableSuffix() === null || $crawlSettings->getDbTableSuffix() === '') ? date('_-_Y-m-d_H-i-s') : $crawlSettings->getDbTableSuffix();
        $crawlSettings->setDbTableSuffix($dbTableSuffix);

        $data = $this->crawlbot->crawlStart($crawlSettings);

        $viewModelVars += [
            'url' => [
                'crawl_iterate' =>
                    $this->router->generateUri(
                        'crawl-iterate',
                        [
                            'id' => $data[EntityCrawlbot::class]->getId(),
                        ]
                    ),
            ],
            'crawlbot' => $data[EntityCrawlbot::class],
        ];

        return new HtmlResponse($this->template->render('crawlbot::index', $viewModelVars));
    }
}

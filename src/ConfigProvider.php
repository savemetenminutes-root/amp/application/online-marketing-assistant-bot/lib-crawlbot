<?php

declare(strict_types=1);

namespace Smtm\Crawlbot;

use Psr\Container\ContainerInterface;
use Smtm\Crawlbot\Form\CrawlFieldsetDecorator;
use Smtm\Crawlbot\Form\CrawlSubmitButtonDecorator;
use Smtm\Crawlbot\Form\DbTableSuffixTextInputDecorator;
use Smtm\Crawlbot\Form\DefaultUrlSchemeSelectDecorator;
use Smtm\Crawlbot\Form\EntryPointUriTextInputDecorator;
use Smtm\Crawlbot\Form\Factory\CrawlFieldsetFactory;
use Smtm\Crawlbot\Form\Factory\FormElementFactory;
use Smtm\Crawlbot\Form\Factory\IndexFormFactory;
use Smtm\Crawlbot\Form\Factory\TranslatorAwareFormElementDelegatorFactory;
use Smtm\Crawlbot\Form\IndexFormDecorator;
use Smtm\Crawlbot\Form\UseCookiesSelectDecorator;
use Smtm\Crawlbot\Handler\CrawlBeginHandler;
use Smtm\Crawlbot\Handler\Factory\CrawlBeginHandlerFactory;
use Smtm\Crawlbot\Handler\Factory\IndexHandlerFactory;
use Smtm\Crawlbot\Handler\IndexHandler;
use Smtm\Crawlbot\Model\Crawlbot as ModelCrawlbot;
use Smtm\Crawlbot\Model\Entity\Crawlbot as EntityCrawlbot;
use Smtm\Crawlbot\Model\Entity\CrawlbotA as EntityCrawlbotA;
use Smtm\Crawlbot\Model\Entity\CrawlbotEntityManager;
use Smtm\Crawlbot\Model\Entity\CrawlbotH1 as EntityCrawlbotH1;
use Smtm\Crawlbot\Model\Entity\CrawlbotH2 as EntityCrawlbotH2;
use Smtm\Crawlbot\Model\Entity\CrawlbotH3 as EntityCrawlbotH3;
use Smtm\Crawlbot\Model\Entity\CrawlbotH4 as EntityCrawlbotH4;
use Smtm\Crawlbot\Model\Entity\CrawlbotH5 as EntityCrawlbotH5;
use Smtm\Crawlbot\Model\Entity\CrawlbotH6 as EntityCrawlbotH6;
use Smtm\Crawlbot\Model\Entity\CrawlbotImg as EntityCrawlbotImg;
use Smtm\Crawlbot\Model\Entity\CrawlbotLink as EntityCrawlbotLink;
use Smtm\Crawlbot\Model\Entity\CrawlbotResponse as EntityCrawlbotResponse;
use Smtm\Crawlbot\Model\Entity\CrawlbotScript as EntityCrawlbotScript;
use Smtm\Crawlbot\Model\Entity\CrawlbotUriCrawled as EntityCrawlbotUriCrawled;
use Smtm\Crawlbot\Model\Entity\CrawlbotUriQueued as EntityCrawlbotUriQueued;
use Smtm\Crawlbot\Model\Entity\Factory\CrawlbotEntityManagerFactory;
use Smtm\Crawlbot\Model\Entity\Hydrator\Factory\CrawlbotGenericClassMethodsHydratorFactory;
use Smtm\Crawlbot\Model\Entity\Hydrator\Factory\CrawlbotUriClassMethodsHydratorDecoratorFactory;
use Smtm\Crawlbot\Model\Factory\CrawlbotFactory as ModelCrawlbotFactory;
use Smtm\Crawlbot\Service\Crawlbot;
use Smtm\Crawlbot\Service\Factory\ClientFactory;
use Smtm\Crawlbot\Service\Factory\CrawlbotFactory;
use Smtm\Http\Client;
use Zend\Expressive\Application;
use Zend\Expressive\Container\ApplicationConfigInjectionDelegator;
use Zend\Expressive\MiddlewareFactory;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\Hydrator\DelegatingHydrator;
use Zend\Hydrator\DelegatingHydratorFactory;
use Zend\I18n\Translator\Translator;
use Zend\I18n\Translator\TranslatorServiceFactory;
use Zend\ServiceManager\Factory\InvokableFactory;
use Zend\View\HelperPluginManager;

class ConfigProvider
{
    public function __invoke() : array
    {
        return [
            'routes' => $this->getRoutesConfig(),
            'dependencies' => $this->getDependenciesConfig(),
            CrawlbotEntityManager::class => $this->getCrawlbotEntityManagerConfig(),
            'form_elements' => $this->getFormElementsConfig(),
            'hydrators' => $this->getHydratorsConfig(),
            'templates' => $this->getTemplatesConfig(),
            'translator' => $this->getTranslationsConfig(),
            'doctrine' => $this->getDoctrineConfig(),
        ];
    }

    public function getRoutesConfig() : array
    {
        return [
            [
                'name'            => 'smtm-crawlbot',
                'path'            => '/smtm-crawlbot[/]',
                'middleware'      => Handler\IndexHandler::class,
                'allowed_methods' => ['GET'],
            ],
            [
                'name'            => 'crawl-begin',
                'path'            => '/smtm-crawlbot/crawl[/]',
                'middleware'      => Handler\CrawlBeginHandler::class,
                'allowed_methods' => ['GET'],
            ],
            [
                'name'            => 'crawl-iterate',
                'path'            => '/smtm-crawlbot/crawl/:id[/]',
                'middleware'      => Handler\CrawlIterateHandler::class,
                'allowed_methods' => ['POST'],
                'options'         => [
                    'constraints' => [
                        'id' => '\d+$',
                    ],
                ],
            ],
        ];
    }

    public function getDependenciesConfig() : array
    {
        return [
            'aliases' => [
                TemplateRendererInterface::class => ZendViewRenderer::class,
            ],
            'factories' => [
                Translator::class => TranslatorServiceFactory::class,
                Client::class => ClientFactory::class,
                DelegatingHydrator::class => DelegatingHydratorFactory::class,
                CrawlbotEntityManager::class => CrawlbotEntityManagerFactory::class,
                ModelCrawlbot::class => ModelCrawlbotFactory::class,
                Crawlbot::class => CrawlbotFactory::class,
                IndexHandler::class => IndexHandlerFactory::class,
                CrawlBeginHandler::class => CrawlBeginHandlerFactory::class,
            ],
            'delegators' => [
                Application::class => [
                    ApplicationConfigInjectionDelegator::class,
                ],
            ],
        ];
    }

    public function getCrawlbotEntityManagerConfig()
    {
        return [
            'shared_by_default' => false,
            'invokables' => [
                EntityCrawlbot::class,
                EntityCrawlbotA::class,
                EntityCrawlbotResponse::class,
                EntityCrawlbotH1::class,
                EntityCrawlbotH2::class,
                EntityCrawlbotH3::class,
                EntityCrawlbotH4::class,
                EntityCrawlbotH5::class,
                EntityCrawlbotH6::class,
                EntityCrawlbotImg::class,
                EntityCrawlbotLink::class,
                EntityCrawlbotScript::class,
                EntityCrawlbotUriCrawled::class,
                EntityCrawlbotUriQueued::class,
            ],
            /*
            'factories' => [
                EntityCrawlbot::class => InvokableFactory::class,
                EntityCrawlbotA::class => InvokableFactory::class,
                EntityCrawlbotResponse::class => InvokableFactory::class,
                EntityCrawlbotH1::class => InvokableFactory::class,
                EntityCrawlbotH2::class => InvokableFactory::class,
                EntityCrawlbotH3::class => InvokableFactory::class,
                EntityCrawlbotH4::class => InvokableFactory::class,
                EntityCrawlbotH5::class => InvokableFactory::class,
                EntityCrawlbotH6::class => InvokableFactory::class,
                EntityCrawlbotImg::class => InvokableFactory::class,
                EntityCrawlbotLink::class => InvokableFactory::class,
                EntityCrawlbotScript::class => InvokableFactory::class,
                EntityCrawlbotUriCrawled::class => InvokableFactory::class,
                EntityCrawlbotUriQueued::class => InvokableFactory::class,
            ],
            */
        ];
    }

    public function getFormElementsConfig()
    {
        return [
            'factories' => [
                DbTableSuffixTextInputDecorator::class => FormElementFactory::class,
                DefaultUrlSchemeSelectDecorator::class => FormElementFactory::class,
                UseCookiesSelectDecorator::class => FormElementFactory::class,
                EntryPointUriTextInputDecorator::class => FormElementFactory::class,
                CrawlFieldsetDecorator::class => CrawlFieldsetFactory::class,
                CrawlSubmitButtonDecorator::class => FormElementFactory::class,
                IndexFormDecorator::class => IndexFormFactory::class,
            ],
            'delegators' => [
                DbTableSuffixTextInputDecorator::class => [
                    TranslatorAwareFormElementDelegatorFactory::class,
                ],
                DefaultUrlSchemeSelectDecorator::class => [
                    TranslatorAwareFormElementDelegatorFactory::class,
                ],
                UseCookiesSelectDecorator::class => [
                    TranslatorAwareFormElementDelegatorFactory::class,
                ],
                EntryPointUriTextInputDecorator::class => [
                    TranslatorAwareFormElementDelegatorFactory::class,
                ],
                CrawlSubmitButtonDecorator::class => [
                    TranslatorAwareFormElementDelegatorFactory::class,
                ],
            ],
        ];
    }

    public function getHydratorsConfig()
    {
        return [
            'factories' => [
                EntityCrawlbot::class => CrawlbotGenericClassMethodsHydratorFactory::class,
                EntityCrawlbotA::class => CrawlbotGenericClassMethodsHydratorFactory::class,
                EntityCrawlbotH1::class => CrawlbotGenericClassMethodsHydratorFactory::class,
                EntityCrawlbotH2::class => CrawlbotGenericClassMethodsHydratorFactory::class,
                EntityCrawlbotH3::class => CrawlbotGenericClassMethodsHydratorFactory::class,
                EntityCrawlbotH4::class => CrawlbotGenericClassMethodsHydratorFactory::class,
                EntityCrawlbotH5::class => CrawlbotGenericClassMethodsHydratorFactory::class,
                EntityCrawlbotH6::class => CrawlbotGenericClassMethodsHydratorFactory::class,
                EntityCrawlbotImg::class => CrawlbotGenericClassMethodsHydratorFactory::class,
                EntityCrawlbotLink::class => CrawlbotGenericClassMethodsHydratorFactory::class,
                EntityCrawlbotScript::class => CrawlbotGenericClassMethodsHydratorFactory::class,
                EntityCrawlbotResponse::class => CrawlbotGenericClassMethodsHydratorFactory::class,
                EntityCrawlbotUriCrawled::class => CrawlbotUriClassMethodsHydratorDecoratorFactory::class,
                EntityCrawlbotUriQueued::class => CrawlbotUriClassMethodsHydratorDecoratorFactory::class,
            ],
        ];
    }

    public function getTemplatesConfig() : array
    {
        return [
            'layout' => 'layout::default',
            'paths' => [
                'crawlbot' => __DIR__ . '/../templates/crawlbot',
                'layout' => __DIR__ . '/../templates/layout',
            ],
        ];
    }

    public function getTranslationsConfig()
    {
        return [
            'locale' => 'en_US',
            'translation_file_patterns' => [
                [
                    'type'     => 'phparray',
                    'base_dir' => __DIR__ . '/../language',
                    'pattern'  => '%s.php',
                ],
            ],
        ];
    }

    public function getDoctrineConfig()
    {
        $localConfigFilename = __DIR__ . '/../../../../config/autoload/doctrine.local.php';
        if(! file_exists($localConfigFilename)) {
            $localConfigFilename = __DIR__ . '/../config/doctrine.local.php';
        }
        if(! file_exists($localConfigFilename)) {
            throw new NoLocalConfigurationException('Local configuration file found neither at ' . realpath(__DIR__ . '/../../../../config/autoload/doctrine.local.php') . ' nor at ' . realpath(__DIR__ . '/../config/doctrine.local.php'));
        }

        return [
            'migrations_configuration' => [
                'omabot_web' => [
                    'directory' => __DIR__ . '/../data/db/migrations',
                    'name'      => 'Doctrine Database Migrations - Online Marketing Assistant Bot',
                    'namespace' => 'Smtm\Crawlbot\Migration',
                    'table'     => 'migrations_omabot_web'
                ],
            ],
            'connection' => require $localConfigFilename,
        ];
    }
}

<?php

namespace Smtm\Crawlbot\Model\Entity;

class CrawlbotUriQueued extends CrawlbotUri
{
    protected $AId;

    /**
     * @return mixed
     */
    public function getAId()
    {
        return $this->AId;
    }

    /**
     * @param mixed $AId
     * @return CrawlbotUriQueued
     */
    public function setAId($AId)
    {
        $this->AId = $AId;
        return $this;
    }
}

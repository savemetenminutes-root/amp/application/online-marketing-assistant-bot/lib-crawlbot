<?php

declare(strict_types=1);

namespace Smtm\Crawlbot\Handler\Factory;

use Psr\Container\ContainerInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Smtm\Crawlbot\Form\IndexFormDecorator;
use Smtm\Crawlbot\Handler\IndexHandler;
use Smtm\Crawlbot\Model\Entity\Crawlbot as EntityCrawlbot;
use Smtm\Crawlbot\Service\Crawlbot;
use Zend\Expressive\Router\RouterInterface;
use Zend\Expressive\Template\TemplateRendererInterface;
use Zend\I18n\Translator\Translator;
use Zend\Router\Http\Segment;

class IndexHandlerFactory
{
    public function __invoke(ContainerInterface $container) : RequestHandlerInterface
    {
        $router   = $container->get(RouterInterface::class);
        $template = $container->get(TemplateRendererInterface::class);
        $translator = $container->get(Translator::class);
        $formManager = $container->get('FormElementManager');
        $indexForm = $formManager->get(IndexFormDecorator::class);
        $crawlbot = $container->get(Crawlbot::class);

        return new IndexHandler($template, $router, $translator, $indexForm, $crawlbot);
    }
}

<?php

namespace Smtm\Crawlbot\Migration;

use Doctrine\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

class Version20191010123500 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $table = $schema->createTable('crawlbot');
        $table->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $table->addColumn('db_table_suffix', 'string', ['length' => 32, 'notNull' => false]);
        $table->addColumn('default_url_scheme', 'smallint', ['notNull' => false]);
        $table->addColumn('use_cookies', 'smallint', ['notNull' => false]);
        $table->addColumn('entry_point_uri', 'binary', ['length' => 4096, 'notNull' => false]);
        $table->setPrimaryKey(['id']);

        $table = $schema->createTable('crawlbot_a');
        $table->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $table->addColumn('crawl_id', 'integer', ['unsigned' => true]);
        $table->addColumn('uri_crawled_id', 'integer', ['unsigned' => true, 'notNull' => false]);
        $table->addColumn('tag_html', 'blob', ['notNull' => false]);
        $table->addColumn('attribute_uri', 'binary', ['length' => 4096, 'notNull' => false]);
        $table->setPrimaryKey(['id']);

        $table = $schema->createTable('crawlbot_h1');
        $table->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $table->addColumn('crawl_id', 'integer', ['unsigned' => true]);
        $table->addColumn('uri_crawled_id', 'integer', ['unsigned' => true, 'notNull' => false]);
        $table->addColumn('tag_html', 'blob', ['notNull' => false]);
        $table->setPrimaryKey(['id']);

        $table = $schema->createTable('crawlbot_h2');
        $table->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $table->addColumn('crawl_id', 'integer', ['unsigned' => true]);
        $table->addColumn('uri_crawled_id', 'integer', ['unsigned' => true, 'notNull' => false]);
        $table->addColumn('tag_html', 'blob', ['notNull' => false]);
        $table->setPrimaryKey(['id']);

        $table = $schema->createTable('crawlbot_h3');
        $table->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $table->addColumn('crawl_id', 'integer', ['unsigned' => true]);
        $table->addColumn('uri_crawled_id', 'integer', ['unsigned' => true, 'notNull' => false]);
        $table->addColumn('tag_html', 'blob', ['notNull' => false]);
        $table->setPrimaryKey(['id']);

        $table = $schema->createTable('crawlbot_h4');
        $table->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $table->addColumn('crawl_id', 'integer', ['unsigned' => true]);
        $table->addColumn('uri_crawled_id', 'integer', ['unsigned' => true, 'notNull' => false]);
        $table->addColumn('tag_html', 'blob', ['notNull' => false]);
        $table->setPrimaryKey(['id']);

        $table = $schema->createTable('crawlbot_h5');
        $table->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $table->addColumn('crawl_id', 'integer', ['unsigned' => true]);
        $table->addColumn('uri_crawled_id', 'integer', ['unsigned' => true, 'notNull' => false]);
        $table->addColumn('tag_html', 'blob', ['notNull' => false]);
        $table->setPrimaryKey(['id']);

        $table = $schema->createTable('crawlbot_h6');
        $table->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $table->addColumn('crawl_id', 'integer', ['unsigned' => true]);
        $table->addColumn('uri_crawled_id', 'integer', ['unsigned' => true, 'notNull' => false]);
        $table->addColumn('tag_html', 'blob', ['notNull' => false]);
        $table->setPrimaryKey(['id']);

        $table = $schema->createTable('crawlbot_img');
        $table->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $table->addColumn('crawl_id', 'integer', ['unsigned' => true]);
        $table->addColumn('uri_crawled_id', 'integer', ['unsigned' => true, 'notNull' => false]);
        $table->addColumn('tag_html', 'blob', ['notNull' => false]);
        $table->addColumn('attribute_uri', 'binary', ['length' => 4096, 'notNull' => false]);
        $table->setPrimaryKey(['id']);

        $table = $schema->createTable('crawlbot_link');
        $table->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $table->addColumn('crawl_id', 'integer', ['unsigned' => true]);
        $table->addColumn('uri_crawled_id', 'integer', ['unsigned' => true, 'notNull' => false]);
        $table->addColumn('tag_html', 'blob', ['notNull' => false]);
        $table->addColumn('attribute_uri', 'binary', ['length' => 4096, 'notNull' => false]);
        $table->setPrimaryKey(['id']);

        $table = $schema->createTable('crawlbot_response');
        $table->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $table->addColumn('crawl_id', 'integer', ['unsigned' => true]);
        $table->addColumn('uri_crawled_id', 'integer', ['unsigned' => true, 'notNull' => false]);
        $table->addColumn('status_code', 'integer', ['notNull' => false]);
        $table->addColumn('headers', 'blob', ['notNull' => false]);
        $table->addColumn('body', 'blob', ['notNull' => false]);
        $table->addColumn('body_size', 'integer', ['notNull' => false]);
        $table->addColumn('body_hash', 'binary', ['length' => 4096, 'notNull' => false]);
        $table->setPrimaryKey(['id']);

        $table = $schema->createTable('crawlbot_script');
        $table->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $table->addColumn('crawl_id', 'integer', ['unsigned' => true]);
        $table->addColumn('uri_crawled_id', 'integer', ['unsigned' => true, 'notNull' => false]);
        $table->addColumn('tag_html', 'blob', ['notNull' => false]);
        $table->addColumn('attribute_uri', 'binary', ['length' => 4096, 'notNull' => false]);
        $table->setPrimaryKey(['id']);

        $table = $schema->createTable('crawlbot_uri_crawled');
        $table->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $table->addColumn('crawl_id', 'integer', ['unsigned' => true]);
        $table->addColumn('uri_hash', 'binary', ['length' => 4096, 'notNull' => false]);
        $table->addColumn('is_mime_content', 'smallint', ['notNull' => false]);
        $table->addColumn('is_java_script', 'smallint', ['notNull' => false]);
        $table->addColumn('is_mail_to', 'smallint', ['notNull' => false]);
        $table->addColumn('content', 'blob', ['notNull' => false]);
        $table->addColumn('is_fragment', 'smallint', ['notNull' => false]);
        $table->addColumn('scheme', 'binary', ['length' => 256, 'notNull' => false]);
        $table->addColumn('user_info', 'binary', ['length' => 4096, 'notNull' => false]);
        $table->addColumn('host', 'binary', ['length' => 4096, 'notNull' => false]);
        $table->addColumn('port', 'binary', ['length' => 32, 'notNull' => false]);
        $table->addColumn('path', 'binary', ['length' => 4096, 'notNull' => false]);
        $table->addColumn('query', 'binary', ['length' => 4096, 'notNull' => false]);
        $table->addColumn('fragment', 'binary', ['length' => 4096, 'notNull' => false]);
        $table->addColumn('host_validator', 'binary', ['length' => 512, 'default' => '', 'notNull' => false]);
        $table->addColumn('ip_validator', 'binary', ['length' => 512, 'default' => '', 'notNull' => false]);
        $table->addColumn('uri_syntax_validator', 'binary', ['length' => 512, 'default' => '', 'notNull' => false]);
        $table->addColumn('is_scheme_relative', 'smallint', ['notNull' => false]);
        $table->addColumn('is_path_relative', 'smallint', ['notNull' => false]);
        $table->addColumn('is_host_ip_address', 'smallint', ['notNull' => false]);
        $table->addColumn('subdomains', 'binary', ['length' => 1024, 'notNull' => false]);
        $table->addColumn('domain', 'binary', ['length' => 512, 'notNull' => false]);
        $table->addColumn('tld', 'binary', ['length' => 256, 'notNull' => false]);
        $table->addColumn('allowed_schemes', 'binary', ['length' => 1024, 'default' => '', 'notNull' => false]);
        $table->addColumn('disallowed_schemes', 'binary', ['length' => 1024, 'default' => '', 'notNull' => false]);
        $table->addColumn('uri_queued_id', 'integer', ['unsigned' => true, 'notNull' => false]);
        $table->addColumn('request_method', 'binary', ['length' => 512, 'notNull' => false]);
        $table->addColumn('request_headers', 'blob', ['notNull' => false]);
        $table->addColumn('request_body', 'blob', ['notNull' => false]);
        $table->addColumn('http_status_code', 'integer', ['notNull' => false]);
        $table->addColumn('response_body_size', 'integer', ['notNull' => false]);
        $table->setPrimaryKey(['id']);


        $table = $schema->createTable('crawlbot_uri_queued');
        $table->addColumn('id', 'integer', ['unsigned' => true, 'autoincrement' => true]);
        $table->addColumn('crawl_id', 'integer', ['unsigned' => true]);
        $table->addColumn('uri_hash', 'binary', ['length' => 4096, 'notNull' => false]);
        $table->addColumn('is_mime_content', 'smallint', ['notNull' => false]);
        $table->addColumn('is_java_script', 'smallint', ['notNull' => false]);
        $table->addColumn('is_mail_to', 'smallint', ['notNull' => false]);
        $table->addColumn('content', 'blob', ['notNull' => false]);
        $table->addColumn('scheme', 'binary', ['length' => 256, 'notNull' => false]);
        $table->addColumn('user_info', 'binary', ['length' => 4096, 'notNull' => false]);
        $table->addColumn('host', 'binary', ['length' => 4096, 'notNull' => false]);
        $table->addColumn('port', 'binary', ['length' => 32, 'notNull' => false]);
        $table->addColumn('path', 'binary', ['length' => 4096, 'notNull' => false]);
        $table->addColumn('query', 'binary', ['length' => 4096, 'notNull' => false]);
        $table->addColumn('fragment', 'binary', ['length' => 4096, 'notNull' => false]);
        $table->addColumn('host_validator', 'binary', ['length' => 512, 'default' => '', 'notNull' => false]);
        $table->addColumn('ip_validator', 'binary', ['length' => 512, 'default' => '', 'notNull' => false]);
        $table->addColumn('uri_syntax_validator', 'binary', ['length' => 512, 'default' => '', 'notNull' => false]);
        $table->addColumn('is_scheme_relative', 'smallint', ['notNull' => false]);
        $table->addColumn('is_path_relative', 'smallint', ['notNull' => false]);
        $table->addColumn('is_host_ip_address', 'smallint', ['notNull' => false]);
        $table->addColumn('subdomains', 'binary', ['length' => 1024, 'notNull' => false]);
        $table->addColumn('domain', 'binary', ['length' => 512, 'notNull' => false]);
        $table->addColumn('tld', 'binary', ['length' => 256, 'notNull' => false]);
        $table->addColumn('allowed_schemes', 'binary', ['length' => 1024, 'default' => '', 'notNull' => false]);
        $table->addColumn('disallowed_schemes', 'binary', ['length' => 1024, 'default' => '', 'notNull' => false]);
        $table->addColumn('a_id', 'integer', ['unsigned' => true, 'notNull' => false]);
        $table->setPrimaryKey(['id']);
    }

    public function down(Schema $schema) : void
    {
        $schema->dropTable('crawlbot');
        $schema->dropTable('crawlbot_a');
        $schema->dropTable('crawlbot_h1');
        $schema->dropTable('crawlbot_h2');
        $schema->dropTable('crawlbot_h3');
        $schema->dropTable('crawlbot_h4');
        $schema->dropTable('crawlbot_h5');
        $schema->dropTable('crawlbot_h6');
        $schema->dropTable('crawlbot_img');
        $schema->dropTable('crawlbot_link');
        $schema->dropTable('crawlbot_response');
        $schema->dropTable('crawlbot_script');
        $schema->dropTable('crawlbot_uri_crawled');
        $schema->dropTable('crawlbot_uri_queued');
    }
}
